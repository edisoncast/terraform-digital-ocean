
provider "digitalocean" {
  token = var.do_token
}

resource "digitalocean_ssh_key" "devsecops" {
  name  = "Deployment key"
  public_key = file("/home/ecastro/Desktop/ejemplo/digital_ocean/devsecops.pub")
}

resource "digitalocean_tag" "default_tag" {
  count = var.conteo_droplets > 0 ? 1 : 0

  name = "DEFAULT:${var.nombre}"
}

resource "digitalocean_droplet" "devsecops" {
    count = var.conteo_droplets

    image  = "ubuntu-20-04-x64"
    name   = format("${var.nombre}-web-%03d", count.index + 1)
    region = var.region
    size   = var.tamano
    private_networking = true

    tags   =  digitalocean_tag.default_tag.*.name
    
    ssh_keys = [digitalocean_ssh_key.devsecops.fingerprint]
    
    connection {

        host = self.ipv4_address
        user = "root"
        type = "ssh"
        private_key = file("/home/ecastro/Desktop/ejemplo/digital_ocean/devsecops")
        timeout = "2m"
    }
 
    provisioner "remote-exec" {
    inline = [
      "export PATH=$PATH:/usr/bin",
      "sudo apt-get update",
      "sudo apt-get -y install nginx"
    ]
  }
}

resource "digitalocean_loadbalancer" "devsecops" {
  count = var.conteo_droplets > 0 ? 1 : 0

  name = "web-lb-devsecops"
  region = var.region

  forwarding_rule {
    entry_port = 80
    entry_protocol = "http"

    target_port = 80
    target_protocol = "http"
  }

  healthcheck {
    port = 22
    protocol = "tcp"
  }
  
   droplet_tag = element(digitalocean_tag.default_tag.*.name, count.index)
}

